import Vue from 'vue'
import App from './App.vue'
import Web3 from 'web3'

Vue.config.productionTip = false

new Vue({
  render: h => h(App, Web3),
}).$mount('#app')
