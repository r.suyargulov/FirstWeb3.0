var http = require('http');
var Web3 = require('web3');
var w3 = new Web3("https://mainnet.infura.io/v3/84940ae43cfd4ab2a6e5697bf948886a");

async function queryBlock(i) {
  var json = await w3.eth.getBlock(i);
  return json;
}

var server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  var blocks = [];

  w3.eth.getBlockNumber().then((n) => {
    console.log(n);
    for (i = (n - 10); i < n; i++) {
      blocks.push(queryBlock(i));
      console.log(i)
    }

    Promise.all(blocks.reverse()).then((value) => {
      res.end(JSON.stringify(value));
    });
    console.log(blocks)
  });
});

server.listen(8000, () => {
  console.log('start localhost:8000');
});