# FirstWeb 3.0

## Установка
```
npm install
```

### Для запуска выполните
```
npm run serve
node index.js
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
